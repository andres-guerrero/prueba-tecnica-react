import {
  REQUEST_TABLE,
  FAILURE_TABLE,
  SUCCESS_TABLE,
  INITIAL_TABLE,
} from '../actions/actions';
import * as initialState from '../states/states';

export default function reducer_states(
  state = initialState.myAplication,
  { type, payload }
) {
  switch (type) {
    case REQUEST_TABLE:
      return handlerTableResultFetchRequest(state);
    case FAILURE_TABLE:
      return handlerTableResultFetchFailure(state);
    case SUCCESS_TABLE:
      const { data } = payload;
      return handlerTableResultFetchSuccess(state, data);
    case INITIAL_TABLE:
      return handlerTableResultFetchInitial(state);
    default:
      return state;
  }
}
//login
const handlerTableResultFetchSuccess = (state, data) => {
  return {
    ...state,
    table_result: {
      status: 200,
      table_result: data,
      loading: false,
    },
  };
};

const handlerTableResultFetchFailure = (state, action) => {
  const temp = state.table_result;
  const { status } = action;
  return {
    ...state,
    table_result: {
      ...temp,
      status,
      loading: false,
    },
  };
};

const handlerTableResultFetchInitial = (state) => {
  const { table_result } = initialState.myAplication;
  return {
    ...state,
    table_result,
  };
};

const handlerTableResultFetchRequest = (state) => {
  return {
    ...state,
    table_Result: {
      ...state.table_Result,
      loading: true,
    },
  };
};
