import { createStore, compose } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers/reducer';
import Content from './components/Content';

const reduxDevTools = () =>
  compose(
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );

const store = createStore(rootReducer, reduxDevTools());

const ContentAplication = () => {
  return (
    <Provider store={store}>
      <Content />
    </Provider>
  );
};

export default ContentAplication;
