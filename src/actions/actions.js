export const REQUEST_TABLE = 'REQUEST_TABLE';
export const FAILURE_TABLE = 'FAILURE_TABLE';
export const SUCCESS_TABLE = 'SUCCESS_TABLE';
export const INITIAL_TABLE = 'INITIAL_TABLE';

export function setRequestTable() {
  return {
    type: REQUEST_TABLE,
  };
}

export function setFailureTable(status) {
  return {
    type: FAILURE_TABLE,
    payload: {
      status,
    },
  };
}

export function setSuccessTable(data) {
  return {
    type: SUCCESS_TABLE,
    payload: {
      data,
    },
  };
}

export function setInitialTable() {
  return {
    type: INITIAL_TABLE,
  };
}
