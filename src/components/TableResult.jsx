import { useState } from 'react';

const headTable = (data) => {
  return Object.getOwnPropertyNames(data[0])
    .filter((item) => item !== 'created_at' && item !== 'updated_at')
    .map((val, i) => {
      return <th key={i}>{val.toLocaleUpperCase()}</th>;
    });
};

const bodyTable = (data) => {
  return (
    <tr key={data.id}>
      <th>{data.id}</th>
      <th>{data.name}</th>
      <th>{data.gender}</th>
      <th>{data.email}</th>
      <th>{data.status}</th>
    </tr>
  );
};

const TableResult = ({ data, colorSelect, arrayColors }) => {
  const [colorTable, setColorTable] = useState(colorSelect);

  function handleClick(event) {
    setColorTable(event.target.value.toString());
  }

  const SelectColor = ({ arrayColors }) => {
    return (
      <select className='form-control' onChange={handleClick}>
        {arrayColors.map((color, i) => (
          <option key={i} value={color}>
            {color}
          </option>
        ))}
      </select>
    );
  };

  return (
    <div className='col-12'>
      <SelectColor arrayColors={arrayColors} />
      <div className='table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl '>
        <table className='table table-striped table-bordered'>
          {data.length > 0 && (
            <>
              <thead className={colorTable}>
                <tr>{headTable(data)}</tr>
              </thead>
              <tbody>
                {data.map((item) => {
                  return bodyTable(item);
                })}
              </tbody>
            </>
          )}
        </table>
      </div>
    </div>
  );
};

export default TableResult;
