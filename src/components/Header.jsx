const Header = ({ handleFetchButton }) => {
  return (
    <nav className='navbar navbar-light bg-light'>
      <form className='form-inline'>
        <button
          className='btn btn-outline-success'
          onClick={handleFetchButton}
          type='button'
        >
          GET USERS
        </button>
      </form>
    </nav>
  );
};

export default Header;
