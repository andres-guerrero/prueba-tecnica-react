import React from 'react';
import {
  setSuccessTable,
  setFailureTable,
  setInitialTable,
} from '../actions/actions';
import { useSelector, useDispatch } from 'react-redux';
import { URL_API } from '../base/constants';
import TableResult from '../components/TableResult';
import Header from '../components/Header';

const Content = () => {
  const dispatch = useDispatch();
  const { table_result } = useSelector((state) => state.table_result);
  const arrayColors = [
    'bg-primary',
    'bg-success',
    'bg-warning',
    'bg-danger',
    'bg-info',
  ];
  let colorSelect = arrayColors[0];

  const handleFetchButton = () => {
    fetch(URL_API)
      .then((res) =>
        res.json().then(function (json) {
          json.code === 200
            ? dispatch(setSuccessTable(json.data))
            : dispatch(setFailureTable(json.code));
        })
      )
      .catch((error) => {
        console.error('Error:', error);
        dispatch(setInitialTable());
      });
  };

  return (
    <div className='container'>
      <Header handleFetchButton={handleFetchButton} />
      <hr />
      <TableResult
        data={table_result}
        colorSelect={colorSelect}
        arrayColors={arrayColors}
      />
    </div>
  );
};

export default Content;
